import networkx as nx
import matplotlib.pyplot as plt

# Load the Zachary's Karate Club graph
G = nx.karate_club_graph()

# Draw the graph
nx.draw(G, with_labels=True)
plt.show()

# Analyze the main characteristics
num_nodes = G.number_of_nodes()
num_edges = G.number_of_edges()
avg_degree = sum(dict(G.degree()).values()) / num_nodes

print(f"Number of nodes (vertices): {num_nodes}")
print(f"Number of edges: {num_edges}")
print(f"Average degree of vertices: {avg_degree}")