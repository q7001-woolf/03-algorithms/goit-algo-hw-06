import networkx as nx

# Create the graph
G = nx.karate_club_graph()

# BFS
def bfs(graph, start, end):
    queue = [(start, [start])]
    while queue:
        (vertex, path) = queue.pop(0)
        for next in set(graph.neighbors(vertex)) - set(path):
            if next == end:
                yield path + [next]
            else:
                queue.append((next, path + [next]))

def dfs(graph, start, end, path=None):
    if path is None:
        path = [start]
    if start == end:
        yield path
    for next in set(graph.neighbors(start)) - set(path):
        yield from dfs(graph, next, end, path + [next])

# Find shortest path using BFS
bfs_paths = list(bfs(G, 0, 33))
bfs_shortest_path = min(bfs_paths, key=len)
print("Shortest path using BFS:", bfs_shortest_path)

# Find shortest path using DFS
dfs_paths = list(dfs(G, 0, 33))
dfs_path = dfs_paths[0] if dfs_paths else None
print("Path using DFS:", dfs_path)