import networkx as nx
import matplotlib.pyplot as plt
import random
import heapq

# Create the Karate Club graph
G = nx.karate_club_graph()

random.seed(42)

# Add random weights to the edges
for (u, v) in G.edges():
    G.edges[u,v]['weight'] = random.randint(1, 10)

# Dijkstra's algorithm
def dijkstra(graph, start, end):
    queue = []
    heapq.heappush(queue, (0, start))
    distances = {node: float('infinity') for node in graph.nodes}
    distances[start] = 0
    shortest_path = {node: None for node in graph.nodes}

    while queue:
        current_distance, current_node = heapq.heappop(queue)

        if distances[current_node] < current_distance:
            continue

        for neighbor in graph.neighbors(current_node):
            weight = graph.edges[current_node, neighbor]['weight']
            distance = current_distance + weight

            if distance < distances[neighbor]:
                distances[neighbor] = distance
                shortest_path[neighbor] = current_node
                heapq.heappush(queue, (distance, neighbor))

    path = []
    while end is not None:
        path.append(end)
        end = shortest_path[end]
    path.reverse()

    return path

# Use the function
source_node = 0  # Change this to your source node
target_node = 33  # Change this to your target node
shortest_path = dijkstra(G, source_node, target_node)
# calculate the total weight of the shortest path
total_weight = 0
for i in range(len(shortest_path)-1):

    weight = G.edges[shortest_path[i], shortest_path[i+1]]['weight']
    print(f"Weight of the edge between node {shortest_path[i]} and node {shortest_path[i+1]} is: {weight}")
    total_weight += weight
print(f"The total weight of the shortest path from node {source_node} to node {target_node} is: {total_weight}")

print(f"The shortest path from node {source_node} to node {target_node} is: {shortest_path}")



# Draw the graph
nx.draw(G, with_labels=True, )
plt.show()