import networkx as nx

G = nx.Graph()

G.add_node("A")


G.add_nodes_from(["B", "C", "D"])

G.add_edge("A", "B")
G.add_edges_from([("A", "C"), ("B", "C"), ("B", "D")])

print(G.nodes())
print(G.edges())
print(list(G.neighbors("A")))  # ['B', 'C']

import networkx as nx
import matplotlib.pyplot as plt

G = nx.Graph()

G.add_node("A")
G.add_nodes_from(["B", "C", "D"])

G.add_edge("A", "B")
G.add_edges_from([("A", "C"), ("B", "C"), ("B", "D")])

num_nodes = G.number_of_nodes()  # 4
num_edges = G.number_of_edges()  # 4
is_connected = nx.is_connected(G)  # True

import matplotlib.pyplot as plt
G = nx.complete_graph(8)
nx.draw(G, with_labels=True)
plt.show()
